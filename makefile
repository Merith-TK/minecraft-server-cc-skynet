default: build
	docker-compose up -d --remove-orphans
up:
	docker-compose up --remove-orphans
down:
	docker-compose down --remove-orphans

build:
	docker-compose build

stop: down
start: up
restart: down default

fix-perms:
	sudo chown 1000:1000 ./* -R