if [ ! -d /data/controller/.git ]; then
	cd /data
	rm -rf /data/controller/
	git clone https://github.com/exa-byte/CCTurtleRemoteController /data/controller
	cd controller
fi

## Fix small webserver error and prevent hardcoding domain
sed -i 's/http:\/\/localhost\///g' data/controller/src/store/useWorld.ts

## change text from localhost to skynet-control
files=$(find . -type f)
for file in $files; do 
	sed -i 's/http:\/\/localhost\//http:\/\/skynet-control\//' $file
done
sed -i 's/const url = "http:\/\/localhost\/"/const url = ""' 
if [ ! -d node_modules ]; then
	npm install
fi
npm run build

## ToDo: Find what I need to check for to see if this command is even needed
#if [ ! -d textures ]; then
	npm run build-textures /data/minecraft.jar /data/mods/
	npm run build-textures /data/minecraft.jar /data/mods/
#fi
npm run server